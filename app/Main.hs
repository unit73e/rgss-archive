module Main where

import Options.Applicative
import qualified Codec.Archive.Rgss as Rgss

data RGSSOptions
  = List FilePath
  | Extract FilePath FilePath
  | Add FilePath [FilePath]

archiveArg :: Parser FilePath
archiveArg = strArgument $ metavar "ARCHIVE"

outputDirOpt :: Parser FilePath
outputDirOpt =
  strOption $
    long "output"
      <> short 'o'
      <> metavar "DIR"
      <> help "Output directory"
      <> value "."

inputFilesArg :: Parser [FilePath]
inputFilesArg =
  many $ strArgument $ metavar "FILE" <> help "Input file or directory"

listCmd :: Parser RGSSOptions
listCmd = List <$> archiveArg

extractCmd :: Parser RGSSOptions
extractCmd = Extract <$> archiveArg <*> outputDirOpt

addCmd :: Parser RGSSOptions
addCmd = Add <$> archiveArg <*> inputFilesArg

listInfo :: ParserInfo RGSSOptions
listInfo = info listCmd $ progDesc "List contents of archive"

extractInfo :: ParserInfo RGSSOptions
extractInfo = info extractCmd $ progDesc "Extracts files from archive"

addInfo :: ParserInfo RGSSOptions
addInfo = info addCmd $ progDesc "Add files to archive"

rgssOpts :: Parser RGSSOptions
rgssOpts =
  subparser $ command "list" listInfo <> command "extract" extractInfo <> command "add" addInfo

rgssInfo :: ParserInfo RGSSOptions
rgssInfo =
  info
    (rgssOpts <**> helper)
    ( fullDesc
        <> progDesc "RGSS file archiver"
        <> header "RGSSArchive - An RGSS file archiver"
    )

main :: IO ()
main = do
  options <- execParser rgssInfo
  case options of
    List a -> Rgss.list a
    Extract a b -> Rgss.extract a b
    Add a bs -> Rgss.add a bs
