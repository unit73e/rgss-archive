module Codec.Archive.Rgss.Key where

import Data.Bits (shiftR, xor)
import Data.Word

-- | RGSS decrypt key.
data Key = Key
  { -- | The key multiplier.
    multiplier :: Word8,
    -- | The key accumulator.
    accumulator :: Word8,
    -- | The key counter.
    counter :: Int,
    -- | The key value.
    value :: Word32
  }
  deriving (Show)

-- | Returns the next value of a key.
nextValue :: Key -> Word32
nextValue k = (value k * mulW32) + accW32
  where
    accW32 = fromIntegral $ accumulator k :: Word32
    mulW32 = fromIntegral $ multiplier k :: Word32

-- | Returns the next key when reading a character.
nextChar :: Key -> Key
nextChar k = k {value = nextValue k}

-- | Returns the next key when reading a byte.
nextByte :: Key -> Key
nextByte k =
  k
    { counter = nextCounter,
      value = if nextCounter == 0 then nextValue k else value k
    }
  where
    nextCounter = (counter k + 1) `mod` 4

-- | Decrypts a character.
decryptChar :: Key -> Word8 -> Word8
decryptChar k c = c `xor` input
  where
    input = fromIntegral $ value k

-- | Decrypts a byte.
decryptByte :: Key -> Word8 -> Word8
decryptByte k c = c `xor` input
  where
    input = fromIntegral $ value k `shiftR` (8 * counter k)

-- | Initial key for RGSS version 1.
keyV1 :: Key
keyV1 =
  Key
    { multiplier = 7,
      accumulator = 3,
      counter = 0,
      value = 0xDEADCAFE
    }

-- | Initial key for RGSS version 2.
keyV2 :: Key
keyV2 = keyV1
