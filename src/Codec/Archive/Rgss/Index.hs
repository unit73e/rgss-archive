module Codec.Archive.Rgss.Index where

import Codec.Archive.Rgss.Types
import Data.Int (Int64)
import Data.Word (Word32)

-- | RGSS archive entry.
data Index = Index
  { -- | the path of the file in the archive.
    path :: Path,
    -- | the size of the file in bytes.
    size :: Size,
    -- | the offset of the file in the archive.
    offset :: Int64,
    -- | the key to decrypt the file.
    key :: Word32
  }
  deriving (Show)
