module Codec.Archive.Rgss.Entry where

import Codec.Archive.Rgss.Types
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text.Lazy as Text
import System.Directory (createDirectoryIfMissing)
import System.FilePath (combine, takeDirectory)

-- | RGSS archive entry.
data Entry = Entry
  { -- | The path of the file within the archive.
    path :: Path,
    -- | The content of the entry.
    bytes :: Bytes
  }

-- | Returns the native file path.
entryPath :: Entry -> FilePath
entryPath = Text.unpack . Text.map toPosix . path
  where
    toPosix c = if c == '\\' then '/' else c

-- | Write a local file based on the entry.
writeFile ::
  -- | The output directory.
  FilePath ->
  -- | The entry to be written.
  Entry ->
  IO ()
writeFile d e = do
  _ <- createDirectoryIfMissing True $ takeDirectory fullpath
  LBS.writeFile fullpath (bytes e)
  where
    fullpath = combine d $ entryPath e
