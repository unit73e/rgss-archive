module Codec.Archive.Rgss.Extract where

import qualified Codec.Archive.Rgss.Entry as Entry
import Codec.Archive.Rgss.GetEntry (getEntries)
import qualified Codec.Archive.Rgss.List as List
import Data.Binary.Get (runGet)
import qualified Data.ByteString.Lazy as LBS

-- Extracts an RGSS archive.
extract ::
  -- The output directory.
  FilePath ->
  -- The input archive.
  FilePath ->
  IO ()
extract d a = do
  bs <- LBS.readFile a
  case List.runOrFail bs of
    Right (_, _, es) -> mapM_ (Entry.writeFile d) $ runGet (getEntries es) bs
    Left (_, _, m) -> print m
