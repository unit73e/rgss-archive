-- |
-- Module      : Codec.Archive.Rgss.Types
-- Copyright   : (c) unit73e, 2023
-- License     : BDS-3-Clause
-- Maintainer  : unit73e@gmail.com
-- Stability   : experimental
-- Portability : portable
--
-- Types to represent the content of RGSS archives.
module Codec.Archive.Rgss.Types where

import qualified Data.ByteString.Lazy as LBS
import Data.Text.Lazy (Text)
import Data.Word (Word32, Word8)

-- | Size of a byte array
--
-- Sizes of byte arrays are stored as a 32-bit unsigned integers.
type Size = Word32

-- | A byte.
--
-- A byte has the common standard, 8-bit unsigned integers.
type Byte = Word8

-- | A byte array.
type Bytes = LBS.ByteString

-- | Archive version.
--
-- The version is stored in one byte.
type Version = Word8

-- | A path.
--
-- Paths in RGSS have UTF-8 encoding, and are usually in Windows style.
type Path = Text
