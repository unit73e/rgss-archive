module Codec.Archive.Rgss.Header where

import Codec.Archive.Rgss.Types

-- | RGSS archive header.
data Header = Header
  { -- | The header prefix.
    prefix :: Bytes,
    -- | The archive version.
    version :: Version
  }
  deriving (Show)
