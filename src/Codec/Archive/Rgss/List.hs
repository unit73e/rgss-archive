module Codec.Archive.Rgss.List where

import Codec.Archive.Rgss.GetIndex (getIndexes)
import qualified Codec.Archive.Rgss.Index as Index
import qualified Codec.Archive.Rgss.Key as Key
import Control.Monad.State (evalStateT)
import Data.Binary.Get (ByteOffset, runGetOrFail)
import qualified Data.ByteString.Lazy as LBS

type Index = Index.Index

runOrFail ::
  LBS.LazyByteString ->
  Either
    (LBS.ByteString, ByteOffset, String)
    (LBS.ByteString, ByteOffset, [Index])
runOrFail bs = do
  let key = Key.keyV1
  runGetOrFail (evalStateT getIndexes key) bs
