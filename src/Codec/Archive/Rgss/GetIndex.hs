module Codec.Archive.Rgss.GetIndex (getIndexes) where

import Codec.Archive.Rgss.GetHeader (getHeader)
import qualified Codec.Archive.Rgss.Index as Index
import qualified Codec.Archive.Rgss.Key as Key
import Codec.Archive.Rgss.Types
import Control.Monad.Combinators (count, many)
import Control.Monad.State (StateT, get, gets, lift, put)
import qualified Data.Binary.Get as Get
import Data.Bits (xor)
import qualified Data.ByteString.Lazy as LBS
import Data.Text.Lazy.Encoding (decodeUtf8)

type GetState = StateT Key.Key Get.Get

type Index = Index.Index

-- | Decrypts the size of a byte aray.
decryptSize :: Key.Key -> Size -> Size
decryptSize k i = i `xor` Key.value k

-- | Reads the size of a byte array.
getSize :: GetState Size
getSize = do
  k <- get
  put $ Key.nextChar k
  lift $ decryptSize k <$> Get.getWord32le

-- | Reads a byte.
getByte :: GetState Byte
getByte = do
  k <- get
  put $ Key.nextChar k
  c <- lift Get.getWord8
  return $ Key.decryptChar k c

-- | Reads a byte array.
getByteArray :: Size -> GetState Bytes
getByteArray i = LBS.pack <$> count (fromIntegral i) getByte

-- | Reads a file path.
--
-- Reads in the following order:
--
-- 1. The size of a byte array.
-- 2. A byte array containing a file path.
--
-- The bytes are decoded in UTF-8.
getFilePath :: GetState Path
getFilePath = do
  i <- getSize
  s <- getByteArray i
  return $ decodeUtf8 s

-- | Reads an index.
--
-- Reads in the following order:
-- 1. The path of a file.
-- 2. The size of the file, in bytes.
-- 3. The offset in the archive, in bytes.
-- 4. The decryption key for the file.
getIndex :: GetState Index
getIndex = do
  name <- getFilePath
  size <- getSize
  offset <- lift Get.bytesRead
  key <- gets Key.value
  lift $ Get.skip $ fromIntegral size
  return $ Index.Index name size offset key

-- | Reads a sequence of indexes.
getIndexes :: GetState [Index]
getIndexes = do
  _ <- lift getHeader
  many getIndex
