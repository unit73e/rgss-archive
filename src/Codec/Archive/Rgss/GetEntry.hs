module Codec.Archive.Rgss.GetEntry (getEntries) where

import qualified Codec.Archive.Rgss.Entry as Entry
import qualified Codec.Archive.Rgss.Index as Index
import qualified Codec.Archive.Rgss.Key as Key
import Codec.Archive.Rgss.Types
import Control.Monad.Combinators (many)
import Control.Monad.State (StateT, evalStateT, get, lift, put)
import Data.Binary (Get, getWord8)
import Data.Binary.Get (bytesRead, getLazyByteString, runGet, skip)
import qualified Data.ByteString.Lazy as LBS
import Data.List (sortOn)
import Prelude hiding (writeFile)

type GetState = StateT Key.Key Get

type Entry = Entry.Entry

type Index = Index.Index

-- | Decrypts a byte
decryptByte :: GetState Byte
decryptByte = do
  k <- get
  put $ Key.nextByte k
  c <- lift getWord8
  return $ Key.decryptByte k c

-- | Decrypts a sequence of bytes
decryptBytes :: Index -> Bytes -> Bytes
decryptBytes e = LBS.pack . runGet (evalStateT bytes key)
  where
    key = Key.keyV1 {Key.value = Index.key e}
    bytes = many decryptByte

-- | Reads an entry
getEntry :: Index -> Get Entry
getEntry e = do
  offset <- bytesRead
  skip $ fromIntegral (Index.offset e) - fromIntegral offset
  bs <- decryptBytes e <$> getLazyByteString (fromIntegral (Index.size e))
  return $ Entry.Entry (Index.path e) bs

-- | Reads a sequences of entries
getEntries :: [Index] -> Get [Entry]
getEntries = mapM getEntry . sortOn Index.offset
