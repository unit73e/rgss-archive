{-# LANGUAGE OverloadedStrings #-}

module Codec.Archive.Rgss.GetHeader (getHeader) where

import qualified Codec.Archive.Rgss.Header as Header
import Codec.Archive.Rgss.Types
import Control.Monad (unless)
import Data.Binary.Get (Get, getLazyByteString, getLazyByteStringNul, getWord8)

type Header = Header.Header

-- | The valid versions.
validVersions :: [Version]
validVersions = [1, 3]

-- | The expected prefix.
expectedPrefix :: Bytes
expectedPrefix = "RGSSAD"

-- | Tests if a a version is valid.
isValidVersion :: Version -> Bool
isValidVersion x = x `elem` validVersions

-- | Reads a prefix.
getPrefix :: Get Bytes
getPrefix = do
  prefix <- getLazyByteString 6 <* getLazyByteStringNul
  unless (prefix == expectedPrefix) $ fail $ "Unexpected header prefix " ++ show prefix
  return prefix

-- | Reads a version.
getVersion :: Get Version
getVersion = do
  version <- getWord8
  unless (isValidVersion version) $ fail $ "Unexpected version " ++ show version
  return version

-- | Reads a header.
--
-- The bytes must have the following format:
-- - A prefix \"RGSSAD\".
-- - A version of one byte, which must be 1 or 3.
getHeader :: Get Header
getHeader = Header.Header <$> getPrefix <*> getVersion
