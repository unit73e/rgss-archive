-- |
-- Module      : Codec.Archive.Rgss
-- Copyright   : (c) unit73e, 2023
-- License     : BDS-3-Clause
-- Maintainer  : unit73e@gmail.com
-- Stability   : experimental
-- Portability : portable
--
-- An archiver for RGSS files.
--
-- Ruby Game Scripting System (RGSS) is a scripting library used in RPG Maker
-- XP, VX, and VX Ace. Usually scripts, graphics, and sound, are packed in
-- encrypted RGSS archives, with the following extensions:
--
-- - RPG Maker XP - `*.rgssad`
-- - RPG Maker VX - `*.rgss2a`
-- - RPG Maker VX Ace - `*.rgss3a`
--
-- This modules has functions to view, extract, archive or manipulate RGSS
-- files.
module Codec.Archive.Rgss
  ( list,
    extract,
    add,
  )
where

import qualified Codec.Archive.Rgss.Extract as Extract
import qualified Codec.Archive.Rgss.Index as Index
import qualified Codec.Archive.Rgss.List as List
import qualified Data.ByteString.Lazy as LBS
import Text.Printf

type Directory = FilePath

printIndex :: Index.Index -> IO ()
printIndex e = printf "%10d %s\n" (Index.size e) (Index.path e)

-- | Lists the files an archive
list ::
  -- | The input archive
  FilePath ->
  IO ()
list a = do
  f <- LBS.readFile a
  case List.runOrFail f of
    Right (_, _, es) -> mapM_ printIndex es
    Left (_, _, m) -> print m

-- | Extracts an archive.
extract ::
  -- | The output directory.
  Directory ->
  -- | The input archive.
  FilePath ->
  IO ()
extract = Extract.extract

add :: FilePath -> [FilePath] -> IO ()
add _ _ = pure ()
