# RGSS Archive

A Ruby Game Script System (RGSS) archiver. Can be used both as an application
or a library.

RGSS files are typically used in:

- RPG Maker XP - data files have `.rgssad` extension.
- RPG Maker VX - data files have `.rgss2a` extension.
- RPG Maker VX Ace - data files have `.rgss3a` extension.

For now only list and extraction is supported and `rgss3a` files are not yet
supported.

## Install

[Cabal][cabal] must be installed. [GHCUp][ghcup] is the recommended way to
install cabal.

Update first:

```sh
cabal update
```

Then install:

```sh
cabal install
```

The installed binary will be here:

```sh
~/.cabal/bin/rgss-archive
```

You may want to add cabal binaries to your path. For example, if you're using
bash:

```sh
# ~/.bash_profile
CABAL_HOME=$HOME/.cabal
PATH=$PATH:$CABAL_HOME/bin
```

## Run

To list files:

```sh
rgss-archive list <input-file>
```

To extract:

```sh
rgss-archive extract <input-file> -o <output-dir>
```

[cabal]: https://www.haskell.org/cabal/
[ghcup]: https://www.haskell.org/ghcup/
